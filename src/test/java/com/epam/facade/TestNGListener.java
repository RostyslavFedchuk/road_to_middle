package com.epam.facade;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(TestNGListener.class);

    public void onTestStart(ITestResult result) {
        LOGGER.info(result.getMethod().getMethodName()+" is starting...");
    }

    public void onTestSuccess(ITestResult result) {
        LOGGER.info(result.getMethod().getMethodName()+" was successfully finished...");
    }

    public void onTestFailure(ITestResult result) {
        LOGGER.error(result.getMethod().getMethodName()+" failed...");
    }

}
