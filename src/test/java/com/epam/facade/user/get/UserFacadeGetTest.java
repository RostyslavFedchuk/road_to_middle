package com.epam.facade.user.get;

import com.epam.facade.TestNGListener;
import com.epam.facade.UserFacade;
import com.epam.facade.user.Preconditions;
import com.epam.model.User;
import com.epam.utils.ResponseValidator;
import io.qameta.allure.Description;
import io.qameta.allure.Issue;
import io.qameta.allure.Link;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

@Listeners({TestNGListener.class})
public class UserFacadeGetTest extends Preconditions {
    private UserFacade userFacade = new UserFacade();

    @Test
    @Link("https://example.org")
    public void getAllUsersHappyPath() {
        List<User> users = userFacade.getUsers();
        users.forEach(ResponseValidator::validateUser);
    }

    @Test
    @Issue("1234")
    public void getUserByIdHappyPath() {
        assertEquals(userFacade.getUserById(createdUser.getId() + ""), createdUser);
    }

    @Test
    @Description("get User By Id Error Handling test")
    public void getUserByIdErrorHandling() {
        assertEquals(userFacade.getUserByIncorrectId("-1").getMessage(), "Resource not found");
    }

}
