package com.epam.facade.user;

import com.epam.facade.UserFacade;
import com.epam.model.User;
import io.qameta.allure.Step;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.epam.utils.DataGenerator.getUser;
import static org.testng.Assert.assertNotNull;

public class Preconditions {
    protected User createdUser;

    @BeforeMethod
    public void createUser() {
        createdUser = new UserFacade().createUser(getUser());
        assertNotNull(createdUser);
    }

    @AfterMethod
    public void deleteUser() {
        new UserFacade().deleteUser("" + createdUser.getId());

    }

}
