package com.epam.facade.user.post;

import com.epam.facade.TestNGListener;
import com.epam.facade.UserFacade;
import com.epam.facade.user.Preconditions;
import io.qameta.allure.Issue;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.epam.utils.DataGenerator.getUser;
import static org.testng.Assert.assertEquals;

@Listeners({TestNGListener.class})
public class UserFacadePostTest extends Preconditions {
    private UserFacade userFacade = new UserFacade();

    @Test
    @Issue("123")
    public void createUserHappyPath() {
        assertEquals(userFacade.getUserById("" + createdUser.getId()), getUser());
        assertEquals(createdUser, getUser());
    }
}
