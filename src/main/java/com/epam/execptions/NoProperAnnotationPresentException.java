package com.epam.execptions;

public class NoProperAnnotationPresentException extends RuntimeException {

    public NoProperAnnotationPresentException(String message){
        super(message);
    }
}
