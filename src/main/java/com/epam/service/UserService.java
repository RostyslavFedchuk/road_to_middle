package com.epam.service;

import com.epam.model.User;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

import static com.epam.utils.Constants.USER_ENDPOINT;

public class UserService extends Service {

    @Step("Getting all users step ...")
    public ValidatableResponse getUsers() {
        return validateResponse(request.get(USER_ENDPOINT));
    }

    @Step("Getting user by id step ...")
    public ValidatableResponse getUserById(String userId) {
        return validateResponse(request
                .get(USER_ENDPOINT + "/" + userId));
    }

    @Step("Adding new user step ...")
    public ValidatableResponse addUser(User user) {
        return validateResponse(authorizedRequest
                .body(user)
                .contentType(ContentType.JSON)
                .post(USER_ENDPOINT));
    }

    @Step("Deleting user by id = {0} step ...")
    public ValidatableResponse deleteUserById(String userId) {
        return validateResponse(request
                .delete(USER_ENDPOINT + "/" + userId));
    }

    @Step("Editing user by id = {0} to new User = {1} step ...")
    public ValidatableResponse editUserById(String userId, User newUser) {
        return validateResponse(request
                .body(newUser)
                .contentType(ContentType.JSON)
                .put(USER_ENDPOINT + "/" + userId));
    }

}
