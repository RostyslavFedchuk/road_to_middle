package com.epam.service;

import com.epam.utils.Constants;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static com.epam.utils.Constants.ACCESS_TOKEN;
import static io.restassured.RestAssured.given;

public abstract class Service {
    protected RequestSpecification request;
    protected RequestSpecification authorizedRequest;

    public Service(){
        request = given()
                .filter(new AllureRestAssured())
                .log()
                .all()
                .baseUri(Constants.BASE_URI);
        authorizedRequest = request.auth().oauth2(ACCESS_TOKEN);
    }

    protected ValidatableResponse validateResponse(Response response){
        return response.then().log().all();
    }
}
