package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Envelop<T> implements Serializable {
    private Integer code;
    private Meta meta;
    private List<T> data = new ArrayList<>();
}
