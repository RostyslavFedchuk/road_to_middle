package com.epam.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Pagination {
    private Integer total;
    private Integer pages;
    private Integer page;
    private Integer limit;
}
