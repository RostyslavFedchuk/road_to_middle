package com.epam.annotations;

import java.lang.annotation.*;

@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyFactory {
    String bundleName() default "config";
}
