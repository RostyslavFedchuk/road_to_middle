package com.epam.facade;

import com.epam.model.Envelop;
import com.epam.model.Error;
import com.epam.model.ErrorEnvelop;
import com.epam.model.User;
import com.epam.service.UserService;
import io.restassured.common.mapper.TypeRef;

import java.util.List;

import static org.apache.http.HttpStatus.*;
import static org.testng.Assert.assertEquals;

public class UserFacade {
    private UserService userService = new UserService();

    public List<User> getUsers() {
        Envelop<User> envelop = userService.getUsers()
                .extract()
                .as(new TypeRef<Envelop<User>>() {
                });
        assert envelop.getCode() == SC_OK;
        return envelop.getData();
    }

    public User getUserById(String userId) {
        return userService.getUserById(userId)
                .extract()
                .jsonPath()
                .getObject("data", User.class);
    }

    public Error getUserByIncorrectId(String userId){
        ErrorEnvelop<Error> envelop = userService.getUserById(userId)
                .extract()
                .as(new TypeRef<ErrorEnvelop<Error>>() {
                });
        assert envelop.getCode() == SC_NOT_FOUND;
        return envelop.getData();
    }

    public User createUser(User user) {
        return userService.addUser(user)
                .extract()
                .jsonPath()
                .getObject("data", User.class);
    }

    public User updateUser(String userId, User user) {
        return userService.editUserById(userId, user)
                .extract()
                .jsonPath()
                .getObject("data", User.class);
    }

    public void deleteUser(String userId) {
        int responseCode = (userService.deleteUserById(userId)
                .extract()
                .jsonPath()
                .getInt("code"));
        assertEquals(responseCode, SC_NO_CONTENT);
    }
}
