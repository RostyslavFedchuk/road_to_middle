package com.epam.utils;

import com.epam.annotations.Property;
import com.epam.annotations.PropertyFactory;

@PropertyFactory
public class Constants {

    @Property(value = "base.uri")
    public static String BASE_URI;

    @Property(value = "endpoint.user")
    public static String USER_ENDPOINT;

    @Property(value = "access.token")
    public static String ACCESS_TOKEN;

    static {
        ConstantsFactory.initFactory(Constants.class);
    }
}
