package com.epam.utils;

import com.epam.model.Gender;
import com.epam.model.User;

import java.time.LocalDateTime;

public class DataGenerator {
    private static final String TIME_APPENDIX = LocalDateTime.now().toString();

    public static User getUser() {
        return User.builder()
                .name("George" + TIME_APPENDIX)
                .email(String.format("George%s@gmail.com", TIME_APPENDIX.replaceAll(":", "")))
                .status("Active")
                .gender(Gender.MALE.getValue())
                .build();
    }
}
