package com.epam.utils;

import com.epam.annotations.Property;
import com.epam.annotations.PropertyFactory;
import com.epam.execptions.NoProperAnnotationPresentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.ResourceBundle;

public class ConstantsFactory {
    private static final Logger LOGGER = LogManager.getLogger(ConstantsFactory.class);

    public static void initFactory(Class clazz) throws NoProperAnnotationPresentException {
        if (clazz.isAnnotationPresent(PropertyFactory.class)) {
            String bundleName = ((PropertyFactory) clazz.getAnnotation(PropertyFactory.class)).bundleName();
            ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
            Field[] fields = clazz.getFields();
            for (Field field : fields) {
                if(field.isAnnotationPresent(Property.class)){
                    String fieldValue = bundle.getString(field.getAnnotation(Property.class).value());
                    setFieldValue(clazz, field, fieldValue);
                }
            }
            LOGGER.trace("Some info...");
        } else {
            LOGGER.error("PropertyFactory annotation is absent!");
            throw new NoProperAnnotationPresentException("PropertyFactory annotation is mandatory!");
        }
    }

    private static void setFieldValue(Object object, Field field, String fieldValue){
        try {
            field.setAccessible(true);
            field.set(object, fieldValue);
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
