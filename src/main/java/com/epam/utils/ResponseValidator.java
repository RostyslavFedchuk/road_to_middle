package com.epam.utils;

import com.epam.model.Gender;
import com.epam.model.User;
import io.qameta.allure.Step;
import org.apache.commons.lang3.EnumUtils;

import static org.testng.Assert.assertTrue;

public class ResponseValidator {

    @Step("Verifying User = {0} step...")
    public static void validateUser(User user) {
        assertTrue(user.getId() > 0, "Incorrect User Id!");
        assertTrue(EnumUtils.isValidEnum(Gender.class, user.getGender().toUpperCase()),
                user.getGender() + " is not a valid Gender");
        verifyEmail(user.getEmail());
        assertTrue(user.getName().matches("^[. a-zA-Z0-9]{3,100}$"), user.getName() + " is not a correct Name!");
    }

    @Step("Verifying Email = {0} step...")
    private static void verifyEmail(String value) {
        assertTrue(value.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$"),
                value + " is not a valid Email");
    }
}
